<div align="center">

# Implementing and Aßessing <br />Unicode Caſe-Inſensitive Matching in <br />ＲＰＬ <br />

Alec Landow and Dr. Jamie Jennings

☃

</div>


[[_TOC_]]

----

## The Regex Way: Down-casing

All regex implementations we could find use down-casing to case-insensitively
match an input string `I` to a pattern `P`.

For Example (using Perl regex-like pseudo-syntax):
```perl
P = "SSS" → "sss"
I = "ssS" → "sss"

I =~ /P/i → "ssS" =~ /SSS/i → "sss" =~ /sss/
```
Both the pattern and the input string are converted to lowercase, and then the
comparisons between characters in the pattern and characters in the string are
simple exact matches.

### Time Complexity
For a pattern `P` of length `N_p` Unicode characters, and an input string `I` of length `N_i` Unicode characters, the worst-case time complexity is determined as follows.

1. **Downcase the Pattern**<br />
    ```
    T_downcase_P = 𝒪(N_p)
    ```
2. **Downcase the Input**<br />
    ```
    T_downcase_I = 𝒪(N_i)
    ```
2. **Compare the Pattern to the Input**<br />
* Best-case: Similar to the Boyer-Moore algorithm if the pattern is only characters, since some regex implementations have this optimization for character-only patterns.
```
T_match_best = 𝒪(N_p + N_i)
T_total_best = 𝒪( 2 * (N_i + N_p) )
```
* Worst-case: Pathological regex involving catastrophic backtracking.
    ```
    T_match_worst = 𝒪(2^N_i)
    T_total_worst = 𝒪(2^N_i + N_i + N_p)
    ```

In either case, pattern compilation is usually insignificant compared to length of input.

----

## The RPL Way: Parsing Expression Grammars Using Case Alternatives

Since RPL implements parsing expression grammars to match patterns to input
strings, case-insensitive matching can take the form of a choice.

For example, to case-insensitively match "a", the following pattern would be used:
```lua
matchApattern = "a" / "A"
```
where `/` is the parsing-expression grammar version of the OR statement without
backtracking.

To match a string case-insensitively, the pattern would consist of a sequence of choices.

For example, to case-insensitively match the string "sss", the following pattern would be used:

```lua
matchStringPattern = {"s" / "S"} {"s" / "S"} {"s" / "S"}
```

The main difficulty lies in how to form a combination of sequences and choices
to make a pattern that will account for all "cases", so to speak.

----
### Previous Case-Insensitive Matching in RPL
Before this update to the case-insensitive RPL matching, RPL only supported
ASCII case-insensitive matching. For a single character in a pattern, a choice
between lowercase and uppercase was formed by first determining the ASCII
code range (e.g. 0x0041 - 0x005A for uppercase and 0x0061 - 0x007A for lowercase),
and then adding or subtracting 0x0020 accordingly to get the other case.

The pseudocode for this ASCII case choice algorithm is (essentially):
```lua
function create_choice(input_char)
    ascii_val = get_ascii_val(input_char)

    if 0x0041 <= ascii_val <= 0x005A then
        other_case = get_char(ascii_val + 0x0020)
    else if 0x0061 <= ascii_val <= 0x007A then
        other_case = get_char(ascii_val - 0x0020)
    else
        return input_char
    end

    return {input_char / other_case}
end
```

----

### Basic Case-Insensitive Matching in RPL

In Unicode the cases of various characters are not separated by a consistent
numerical value. In the case of a mapping of a single character to another
single character of a different case variant (one-to-one for short), the main
question is: how can we get the Unicode case mappings?

The distinction of one-to-one, as opposed to one-to-many and many-to-one, is important because for full Unicode case-folding an equivalence between multiple characters and single characters must be established, and doing this is not straightforward. (A good example is Unicode sharp s, `ß`, which must match with two consecutive ASCII s characters, `ss`, in addition to matching with capital sharp s, `ẞ`, and combinations of `s` and Unicode long s, `ſ`, which to many of us looks like an ASCII `f`.)

We denote basic case-insensitive matching as matching any two characters that are
case basic variants of each other, where the basic case variants are:
* lowercase (e.g. `a`)
* uppercase (e.g. `A`)
* titlecase (e.g. `A`, which here is identical to uppercase)

Titlecase is defined as "A mixed-case style with all words capitalised, except for certain subsets (particularly articles and short prepositions and conjunctions) defined by rules that are not universally standardised." [[1](https://en.wikipedia.org/wiki/Letter_case#Title_case)] The following movie name is an example of titlecase usage:
```
The Lord of the Rings
```
where `T`, `L`, and `R` are titlecase variants. For all Unicode case mappings (as of UCD-10.0.0),
there are only four titlecase characters that are not identical to their corresponding
uppercase variants:

|lowercase | uppercase | titlecase |
| ---      | ---       | ---       |
| ǳ        | Ǳ         | ǲ         |
| ǆ        | Ǆ         | ǅ         |
| ǉ        | Ǉ         | ǈ         |
| ǌ        | Ǌ         | ǋ         |

We obtained the basic mappings from [UnicodeData.txt](https://gitlab.com/rosie-pattern-language/rosie/-/blob/master/src/unicode/UCD-10.0.0/UnicodeData.txt), which is available from the Unicode Consortium. Each line in UnicodeData.txt contains information for a specific codepoint, starting at 0x0000 and continuing up to a max codepoint value of 0xE01EF, but omitting large chunks of CJK (Chinese, Japanese, Korean) character codepoints. A single line contains the following information, each value delimited by a semicolon:

0. Codepoint in hex
1. Name
2. General Category (Enumeration)
3. Canonical Combining Class
4. Bidi Class (Enumeration)
5. Decomposition Type and Mapping
6. Numeric Type and Value
7. Numeric Type and Value
8. Numeric Type and Value
9. Bidi Mirrored (Binary)
10. Obsolete
11. Obsolete
12. Simple Uppercase Mapping (Codepoint)
13. Simple Lowercase Mapping (Codepoint)
14. Simple Titlecase Mapping (Codepoint)

For example, for character lowercase `a`:
```
0061;LATIN SMALL LETTER A;Ll;0;L;;;;;N;;;0041;;0041
```
For our purposes we were only interested in fields 0, 12, 13, and 14. To extract this data we used RPL patterns.

#### The Case-Mapping Table
One of the initial questions we sought to answer was: what is a way for RPL to be able to access the mappings needed to create an abstract syntax tree composed of the appropriate case choices? It would take an unnecessarily long amount of time to parse UnicodeData.txt (which has 31,618 lines and is approximately 1.7 MB) each time a new RPL pattern was compiled. (Imagine having to parse this file each time you used a simple regular expression.)

So, the solution is to persist the data by first parsing UnicodeData.txt at build time, and then saving the results to a file in an easily-reusable format.

##### Mark 1
The first iteration consisted of writing the case mappings to a binary file for which each line had the following format:
```
[key - 4 bytes] [lowercase codepoint - 4 bytes] [uppercase codepoint - 4 bytes] [titlecase codepoint - 4 bytes]
```
Each Unicode character can be represented int UTF-8 by a maximum of 4 bytes, hence the 4 bytes reserved for each field. For example, the line for the mapping of lowercase `a` looked like (represented in hexadecimal, with the vertical bars added for readability):

```
0000 0000 0000 0061 | 0000 0000 0000 0061 | 0000 0000 0000 0041 | 0000 0000 0000 0041
```
*Mapping for `a`*


While there were 31,618 lines in UnicodeData.txt, the maximum codepoint value was 0xE01EF, or 917,999 in decimal. Given an input character (e.g. `a`, codepoint 0x0061), we would need to be able to look up its mappings quickly. Since the codepoints in UnicodeData.txt are not all consecutive and have large gaps, a binary search would be needed. In order to enable constant time lookup, we opted in this first iteration to include null mappings for codepoints that were absent from UnicodeData.txt, which would enable a single on-disk search, or even indexing into an array of all the data pulled in to memory.

The tradeoff here was that the case mapping file was very large:
```
(917,999 lines) * 4 fields * 4 bytes ≈ 16 MB
```
There was a lot of space that was wasted since 4 bytes were reserved for all codepoints, but most of the codepoints that had case mappings were encoded with fewer than 4 bytes.

##### Mark 2

Since there were on the order of approximately 2,000 characters that had basic case mapping, it would be ideal to take advantage of this smaller number, if possible.

Since Lua is the language we use to construct that AST (abstract syntax tree) for each RPL pattern, we decided to store the case mappings in pre-compiled .luac files that were simple lua tables that contained the mappings. For example, the entry for lowercase `a` was (before compilation to .luac):
```lua
[97] =
{
    ["title"] = 65,
    ["upper"] = 65,
},

```
where the mappings are stored as decimal values.

This .luac file was only 141 kB, a big improvement over 16 MB!

#### Creating the AST

With the case mappings readily at hand, for a string `I` the creation of the AST followed the algorithm:
```
lookup_mapping(I[1]) lookup_mapping(I[2]) ... lookup_mapping(I[n])
```

or in pseudocode (again displaying only the essentials here):

```lua
function create_basic_AST(input_string):
    sequence = ''
    foreach char in input_string do:
        char_codepoint = get_codepoint(char)
        choices = {char, mapping_table[char_codepoint]}
        sequence.append(choices)
    end

    return sequence
end
```

#### Time Complexity
For a pattern `P` of length `N_p` Unicode characters, and an input string `I` of length `N_i` Unicode characters, the worst-case time complexity is determined as follows.

1. **Compile Time: Form the AST**<br />
Each character will have a maximum of three possible choices (including itself), but looking up the mapping for each character will take constant time:
    ```
    T_AST = 𝒪(N_p)
    ```
2. **Run Time: Execute the compiled-AST virtual machine instructions of RPL**<br />
In the worst case, each node of the AST is traversed, and the worst-case AST is a full ternary tree.
    ```
    T_execute = 𝒪( N_nodes_AST               * N_i )
                𝒪( ( [3^(N_p + 1) - 3] / 2 ) * N_i )
    ```

```
T_total = 𝒪( 3 * 3^N_p * N_i / 2 + N_p)
```

Comparing the above result to the time complexity of regex down-casing,
```
T_total_best  = 𝒪( 2 * (N_i + N_p) )
T_total_worst = 𝒪(2^N_i + N_i + N_p)
```
if `N_i ≫ N_p`, we have:

```
T_regex_best  = 𝒪(2 * N_i)
T_regex_worst = 𝒪(2^N_i)
T_RPL         = 𝒪( (3 / 2) * N_i )
```

#### Testing
Using [Aristotle's Nicomachean Ethics, Volume 1](http://www.gutenberg.org/files/28626/28626-0.txt), which consists mostly of Unicode Greek characters, a comparison of case-insensitive search between Perl and RPL gave the following results:
* TODO: add word count, byte count

```
perl: (?^uix:(\N{U+03B4})) 0.072546 s
Wide character in printf at case-timing.pl line 116.
adjusted rpl  : 'findall:ci:{"δ"}'   0.017698 s
Wide character in printf at case-timing.pl line 121.
rpl match time: 'findall:ci:{"δ"}'   0.016159 s

perl: (?^uix:(\N{U+03B1})) 0.092184 s
Wide character in printf at case-timing.pl line 116.
adjusted rpl  : 'findall:ci:{"α"}'   0.031195 s
Wide character in printf at case-timing.pl line 121.
rpl match time: 'findall:ci:{"α"}'   0.016553 s

perl: (?^uix:(\N{U+0391})) 0.096497 s
Wide character in printf at case-timing.pl line 116.
adjusted rpl  : 'findall:ci:{"Α"}'   0.016281 s
Wide character in printf at case-timing.pl line 121.
rpl match time: 'findall:ci:{"Α"}'   0.013176 s
```

RPL consistently outperformed Perl by one order of magnitude.

----

### Case-Folding in RPL
According to the Unicode Consortium, case folding is a transformation of input text, mostly to lowercase variants, that is intended for internal processing [[2](https://unicode.org/faq/casemap_charprop.html)], especially for international and special-case characters. For example, if Perl were to case-insensitively match one of these characters, it would case-fold the pattern and the input string instead of simply down-casing both.

According to the case-folding mappings provided by the Unicode Consortium in [CaseFolding.txt](https://gitlab.com/rosie-pattern-language/rosie/-/blob/master/src/unicode/UCD-10.0.0/CaseFolding.txt), there are four types of case folding:
* **C: Common Case Folding**<br />
Common mappings shared by both simple and full mappings

* **S: Simple Case folding**<br />
Mappings to single characters where different from F.

* **F: Full Case Folding**<br />
Mappings that cause strings to grow in length. Multiple characters are separated by spaces.


* **T: Special Case for Uppercase I and Dotted Uppercase I**
    - For non-Turkic languages, this mapping is normally not used.
    - For Turkic languages (tr, az), this mapping can be used instead of the normal mapping for these characters.
      Note that the Turkic mappings do not maintain canonical equivalence without additional processing.
      See the discussions of case mapping in the Unicode Standard for more information.

The mappings in CaseFolding.txt are of the following form:
```
<codepoint>; <mapping type (a.k.a. status)>; <mapping codepoint>; # <name>
```

----

#### Simple Case-Folding
CaseFolding.txt prescribes implementing simple case folding by first applying the common mapping, then the simple mapping.

Since simple case folding is a one-to-one mapping, we have not yet focused on it because its implementation is almost identical to basic case-insensitive matching. The main difference is the creation of the mapping table to use. We describe the method for creating the full case-folding mapping table below, and that method will be generalized to apply to simple case-folding in the near future.

----

#### Full Case-Folding
The implementation of full case folding is more complicated than the basic case-insensitive matching and simple case folding, both of which deal only with one-to-one mappings. Now, with full case-folding, we have to handle the cases of one-to-many and many-to-one.

One-to-many is very similar to the method described in basic case-insensitive matching, the main difference being that the case variants consist of multiple characters in sequence instead of just choices of single characters. For example, the mapping of "Latin Small Letter Sharp S", ß, would be a one-to-many mapping (as well as a one-to-one mapping):

```lua
choice("ß") =  "ß" / "ẞ"  / "ſs" / "sſ"
            / "ss" / "SS" / "sS" / "Ss"
            / "ſſ" / "SS" / "ſS" / "Sſ"
```

However, many-to-one needs to be handled differently. For example, the mapping of two lowercase "s" characters, needs to be interpreted to allow for the following possibilities (which are the same as the choices for `ß`):

```lua
choice("ss") =  "ß" / "ẞ"  / "ſs" / "sſ"
             / "ss" / "SS" / "sS" / "Ss"
             / "ſſ" / "SS" / "ſS" / "Sſ"
```

The AST cannot be formed by forming choices from one character of the pattern at a time. Otherwise, how would we look up the "ss" mapping? Thankfully, CaseFolding.txt contains only two-to-one mappings and three-to-one mappings (in addition to the one-to-one mappings). Somehow we need to form the AST by examining one character at a time, then two characters at a time, then three characters at time.

##### The Full Case-Mapping Table
First, we need to have the full case-mapping table, the analog to the basic case-mapping table. To do this, we first parsed CaseFolding.txt similarly to how we parsed UnicodeData.txt, but with the semicolon-delimited mapping described above.

We then flipped the mappings, since CaseFolding.txt only provides one-way mappings to lowercase variants. As we will soon see, only the mappings for which the lowercase variant is the key will be of use to us.

Lastly, to each key of the case-mapping table we added all possible permutations based on the original basic case mapping table. For example, after the first pass, the mapping of `ß` would be:

```lua
choice("ss") =  "ß" / "ẞ" / "ss"
```
After the second pass, it would be
```lua
choice("ss") =  "ß" / "ẞ"  / "ss" / "sS" / "Ss" / "SS"
```
This addition of permutations was first implemented in a naīve way that just dumped in as many possible permutations as were available, and created new mappings from these permutations. For example, the following mapping was created:
```lua
choice("SS") =  "ß" / "ẞ" / "SS"
```

Notice also that this naīve approach created many-to-many mappings, which we do not need because those cases will be covered by the one-to-one mappings combined with the many-to-one and one-to-many.

**The important point here is that we do have the many-to-one mappings,
and we do have the original one-to-one case mappings, both of which we will need.**

##### Creating the AST
The initial goal was to perform full case matching without case folding either the pattern or the input string.

Let's start with the example of a pattern that consists of all lowercase characters:
```lua
P = "sss"
```
This pattern would need to match each of the following input strings
(we ignore ſ in this example for the sake of simplicity:
```lua
   "sss" / "ssS" / "sSs" / "sSS" / "sß" / "sẞ"
 / "Sss" / "SsS" / "SSs" / "SSS" / "Sß" / "Sẞ"
 / "ßs"  / "ßS"  / "ẞs"  / "ẞS"
```

Starting with the first character of the pattern, we apply the case-mapping table:
```lua
{"s" / "S"} to_be_parsed("ss")
```
But, we also need to consider what would happen if we look at the first two characters
and find their mapping, so the AST will be a choice between looking at the first character
and looking at the first two characters:
```lua
( {"s" / "S"} to_be_parsed("ss") )
/
( {"ß" / "ẞ"} to_be_parsed("s") )
```
Repeating the same pattern with the "to_be_parsed" pieces of the pattern, we get:
```lua
( {"s" / "S"} {
   ( {"s" / "S"} {"s" / "S"} )
   /
   {"ß" / "ẞ"}
} )
/
( {"ß" / "ẞ"} {"s" / "S"} )
```

This is a recursive algorithm, and can be expressed in essential pseudocode as:
```lua
local function create_full_AST(input_string)
    if length(input_string) == 0 then
        return ""
    end

    local choices = list.new()
    for i = 1, 2, 3; if i <= length(input_string) do
        part_1, part_2 = chop_string_at(input_string, i) -- split the input string at index i
        choices.append(
            sequence( full_mapping_table(part_1), create_full_AST(part_2) )
        )
    end

    return choices.split(" / ")
end

```

The above algorithm will work for all patterns that consist of only lowercase characters.

Consider, however, this pattern:
```lua
P = "ßs"
```
"ßs" will need to match "sß". But how will that happen? There is no direct mapping
for that match. Implicit in this match is the understanding that `ß` represents `ss`.
Normal case-folding algorithms fold both the pattern and the input string in order to
get patterns and inputs like "ßs" and "sß" to match.

We will get "ßs" and "sß" to match by case folding as well, but only case folding of the pattern.
If the full case-folding rules prescribed in CaseFolding.txt are applied to "ßs",
then the pattern becomes
```
P = "sss"
```
and we are back to our first example, which we demonstrated to be solvable with
the recursive algorithm. Full case folding is done by having four pre-constructed tables,
one for each type of mapping.

As mentioned previously, many of the permutations added in the full case-mapping table
are unnecessary since only lowercase (i.e. fully-folded) character combinations will be
used as keys. One optimization to make would be to simplify the full case-mapping table.

Since real-life input strings are likely to be much longer than patterns,
we still expect significant benefit from this approach.

##### Time Complexity
For a pattern `P` of length `N_p` Unicode characters, and an input string `I` of length `N_i` Unicode characters, the worst-case time complexity is determined as follows.

1. **Compile Time: Full-Case-Fold the Pattern**<br />
Since each character can be fully case-folded in constant time,
    ```
    T_folding = 𝒪(N_p)
    ```
2. **Compile Time: Recursively Create the AST**<br />
We will assume the worst case scenario of the pattern having a one-to-one, two-to-one, and three-to-one mapping
for each singlet, doublet, and triplet sub-pattern, respectively.

    For the first pass we will have the following choices:

    ```lua
      {one-to-one mapping}   to_be_parsed(N_p - 1)
    / {two-to-one mapping}   to_be_parsed(N_p - 2)
    / {three-to-one mapping} to_be_parsed(N_p - 3)
    ```

    For the second pass we will have the following choices for the first line alone:

    ```lua
    {one-to-one mapping} {
        {one-to-one mapping}   to_be_parsed(N_p - 1 - 1)
      / {two-to-one mapping}   to_be_parsed(N_p - 1 - 2)
      / {three-to-one mapping} to_be_parsed(N_p - 1 - 3)
    }
    ```

    The largest number of recursive calls will occur for the one-to-one mappings,
    since only one character is removed each time. Each recursive call leads to three
    lookups. Therefore
    ```
    T_AST = 𝒪(3^N_p)
    ```

3. **Run Time: Execute the compiled-AST virtual machine instructions of RPL**<br />
* TODO: describe the choices as being at the boundaries of chars
    In the worst case, each leaf of the AST, which will also be of size `𝒪(3^N_p)`, will be traversed for each character of input:
    ```
    T_walk = 𝒪(3^N_p * N_i)
    ```

    ```
    T_total = 𝒪(3^N_p * N_i + 3^N_p + N_p)
            = 𝒪(3^N_p * N_i)
    ```


Comparing the above result to the time complexity of regex down-casing,
`𝒪( 2 * (N_i + N_p) ) `, if `N_i ≫ N_p`, we have:
```
T_regex = 𝒪(2 * N_i)
T_RPL   = 𝒪(N_i)
```

which is the same result as that for basic case-insensitive matching with RPL.

##### Testing

###### Unit tests
Unit tests used the following permutations of s, S, ß, ẞ, and ſ:
```lua
    "sss", "ssS", "sSs", "sSS",
    "SSS", "Sss", "SSs", "SsS",
    "ſſſ", "ſſS", "ſSſ", "ſSS",
    "SSS", "Sſſ", "SSſ", "SſS",
    "ſſs", "ſsS", "ſSs", "ſSS",
    "Sſs", "ſss", "ſsſ", "ssſ",
    "ßs" , "sß" , "ßS", "Sß",
    "ẞs", "sẞ" , "ẞS", "Sẞ",
    "ßſ" , "ſß" , "ẞſ", "ſẞ"
```
Each permutation was used the pattern and compared for equivalence with all other
permutations. All tests passed.

## Future Work

### Speed Up by Factoring out Common Byte Sequences

### C Vector Instrinsic Functions

### Persisting Basic Pre-Compiled RPL Patterns

### Canonical Equivalence
é == e + '

<!-- <details>
<summary>Click this to collapse/fold.</summary>

These details <em>will</em> remain <b>hidden</b> until expanded.

<pre><code>PASTE LOGS HERE</code></pre>

</details> -->
