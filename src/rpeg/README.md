## What is rpeg?

The name `rpeg` describes the much-modified variant of `lpeg` on which Rosie is
built.  Rosie began in 2015 as a layer on top of `lpeg` but has since grown to
require many changes to the `lpeg` implementation.  While most of the file names
in the `compiler` and `runtime` sub-directories retain their old `lpeg` names,
the implementation has changed in significant ways.

The `rpeg` directory contains C code that implements several key parts of the
Rosie implementation:

* The back-end of the RPL compiler, where RPL expressions are represented as
  trees using (essentially) the same format as `lpeg`.  Rosie was originally
  based on `lpeg` but has diverged from it significantly.
  
* The runtime for Rosie, which is a "pattern matching virtual machine" that is
  derived directly from the `lpeg` project.  Except, the `lpeg` implementation
  leverages Lua code extensively (e.g. to use `table` data structures), the
  Rosie `rpeg` implementation is somewhat less dependent on Lua.  One of the
  goals for a future "Rosie 2.0" is to have a runtime that:
  
  1. does not require Lua source code to build;
  2. can be built separately from the RPL compiler; and
  3. is extensible via a plug-in interface.
  
* The interface between `rpeg` and Lua is still in `compiler/lptree.c`, and
  looks quite similar to its origin in `lpeg`.  However, there are some
  important changes, most notably that it is now possible to do matching
  (i.e. invoke the Rosie "pattern matching vm") without copying the input data.
  This is important for overall performance.

## The sub-directories

* `compiler` contains the back-end of the RPL compiler and the interface to Lua,
  so that the front-end (written in Lua) can invoke the back-end.  Also, the CLI
  (written in Lua) needs that Lua interface to invoke runtime functions.  The
  entire interface between `rpeg` and Lua is in `lptree.c`, including the
  interface to the runtime.  Eventually, we want to build the compiler and
  runtime separately, and the Lua interface will be separated as well.
  
* `runtime` contains the "pattern matching vm" implementation and supporting
  code.
  
* `future` contains code that is not needed to build Rosie.  There are some
  experiments in ahead-of-time compilation of RPL patterns (which will become a
  supported feature in "Rosie 2.0"), and there is some performance analysis code
  as well.  Nothing in `future` is documented, tested, or necessary to build
  Rosie.
  
  
