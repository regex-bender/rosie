--------------------------------------------------------------------------------
-- Full Case Folding
--------------------------------------------------------------------------------
local cff = {}

local ci = import "BUILTINS-macro-ci"
local unicode_util = import "unicode-util"

local C_case_mapper    = import "UNICODE-case-folding-table-C"
local F_case_mapper    = import "UNICODE-case-folding-table-F"
local full_case_mapper = import "UNICODE-reverse-case-folding-table-CF.lua"


function test1(sub_exp)
    for _, codepoint_decimal in utf8.codes(sub_exp) do
        print(codepoint_decimal)
    end
end


function get_full_cases(sub_exp)
    local cases = {sub_exp} -- sub_exp is a string
    local list_of_codepoints_hex = {}

    -- print("sub_exp = " .. sub_exp)
    for _, codepoint_decimal in utf8.codes(sub_exp) do
        list_of_codepoints_hex[#list_of_codepoints_hex + 1] = string.format("%04X", codepoint_decimal)
    end

    local key = table.concat(list_of_codepoints_hex, " ")
    -- print("key: " .. key)

    local case_map = full_case_mapper[key]

    if (case_map ~= nil) then
        for subkey, placeholder_value in pairs(case_map) do -- (placeholder_value is always 1 b/c case_map is a set)
            cases[#cases + 1] = unicode_util.get_utf8_chars(subkey)
        end
    end
    -- print("case map:")
    -- table.print(cases)

    return cases
end



-- The argument, char, MUST be a string containing one UTF-8 encoded character
-- or a single byte (which will be in 0x80-0xFF).
local function generate_cff_ast_for(sub_exp, sref)
    -- A literal is stored in its escaped format, so we must escape it as we
    -- create literals.  TODO: Do not store literals in escaped format.
    -- fetch_cases_for_char(char) will index into large file and return a list
    -- (really a Lua "array") of 1 or more strings, each of which is a valid case
    -- for the char argument.

    --[[ TODO: implement full case-fold sub_exp here ]]--
    local cases = get_full_cases(sub_exp)
    local escaped_cases = list.map(ustring.escape_string, cases) --[[ TODO: will this work for multi-char strings? ]]--
    local char_literals = list.map(
            function(escaped_case)
                return ast.literal.new{value=escaped_case, sourceref=sref}
             end, escaped_cases
    )

    assert(#char_literals > 0)

    if #cases == 1 then
       return char_literals[1]    -- no "choice" structure needed
    else
       return ast.choice.new{exps=char_literals, sourceref=sref}
    end
end


local function chop_exp_at(exp_value, index)
    -- print("exp_value = " .. exp_value)
    local length = utf8.len(exp_value)
    -- print("length = " .. length)
    -- print("str.len = " .. string.len(exp_value))
    local part_1_stop = utf8.offset(exp_value, index + 1)
    -- print("part_1_stop = " .. part_1_stop)
    local part_1 = exp_value:sub(1, part_1_stop - 1)
    -- print("part_1 = " .. part_1)

    local part_2
    if index + 1 <= length then
        local part_2_start = utf8.offset(exp_value, index + 1)
        part_2 = exp_value:sub(part_2_start);
    else
        part_2 = ""
    end

    return part_1, part_2
end


local function to_cff_literal_helper(exp_value, sourceref)
    local length = utf8.len(exp_value)
    if length == 0 then
        return ast.literal.new{value = '', sourceref = sourceref}
    end

    local disjunctions = list.new()
    for i = 1, math.min(3, length) do -- only look at the first 1 char, 2 chars, or 3 chars in exp.value
        local part_1
        local part_2
        part_1, part_2 = chop_exp_at(exp_value, i)
        -- print("part_1 = " .. part_1)
        -- print("part_2 = " .. part_2)
        table.insert(disjunctions, ast.sequence.new{
            exps = {
                generate_cff_ast_for(part_1, sourceref),
                to_cff_literal_helper(part_2, sourceref)
            },
            sourceref = sourceref
        })
    end

    assert(#disjunctions > 0)
    -- table.print(disjunctions) -- for debugging
    return ast.choice.new{ exps = disjunctions, sourceref = sourceref }
end


local function to_cff_literal(exp)
    -- local fully_folded_string = S_case_mapper[]
    -- print("exp.value = " .. exp.value)

    -- Perform case folding per the rules defined in UCD-10.0.0/CaseFolding.txt
    local full_case_folded_value = unicode_util.case_fold(exp.value, C_case_mapper)
    full_case_folded_value = unicode_util.case_fold(full_case_folded_value, F_case_mapper)
    -- print("full_case_folded_value = " .. full_case_folded_value)

    return ast.raw.new{
        exp = to_cff_literal_helper(full_case_folded_value, exp.sourceref),
        sourceref = exp.sourceref
    }
end


-- to_case_insensitive: named charsets
local function to_cff_named_charset(exp)
   local other_case
   if exp.name == 'upper' then
      other_case = 'lower'
   elseif exp.name == 'lower' then
      other_case = 'upper'
   end
   if other_case then
      local upper_and_lower =
     {exp,
      ast.cs_named.new{complement = exp.complement,
               name = other_case,
               sourceref = exp.sourceref}}
      return ast.choice.new{exps = upper_and_lower, sourceref = exp.sourceref}
   end
   -- else we have a named set that has no other case
   return exp
end


-- to_case_insensitive: character lists
-- FUTURE: flatten the choices?
-- TODO: apply case folding
local function to_cff_list_charset(exp)
   local disjunctions = list.new()
   for _, char in ipairs(exp.chars) do
      table.insert(disjunctions, generate_cff_expression_for_char(char, exp.sourceref))
   end -- for each char in the input literal
   return ast.raw.new{exp=ast.choice.new{exps=disjunctions, sourceref=exp.sourceref},
              sourceref=exp.sourceref}
end


-- to_case_insensitive: character ranges
local function to_cff_range_charset(exp)
   return ci.to_ci_range_charset(exp)
end


function cff.macro_full_case_fold(...)
   local args = {...}
   if #args~=1 then error("cff takes one argument, " .. tostring(#args) .. " given"); end
   local exp = args[1]
   local retval = ast.visit_expressions(exp, ast.literal.is, to_cff_literal)
   retval = ast.visit_expressions(retval, ast.cs_named.is  , ci.to_ci_named_charset)
   retval = ast.visit_expressions(retval, ast.cs_list.is   , ci.to_ci_list_charset)
   retval = ast.visit_expressions(retval, ast.cs_range.is  , ci.to_ci_range_charset)
   return retval
end


return cff
